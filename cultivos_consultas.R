library(dplyr)

# Para descarga del SIAP
# for i in $(seq 2003 2021); \ 
# do wget \ 
# $(printf http://infosiap.siap.gob.mx/gobmx/datosAbiertos/ProduccionAgricola/Cierre_agricola_mun_%s.csv $i); done

nombres = c("anio", "idestado", "nomestado", "idddr", "nomddr", "idcader", 
            "nomcader", "idmunicipio", "nommunicipio", "idciclo", "nomcicloproductivo", 
            "idmodalidad", "nommodalidad", "idunidadmedida", "nomunidad", 
            "idcultivo", "nomcultivo", "sembrada", "cosechada", "siniestrada", 
            "volumenproduccion", "rendimiento", "precio", "valorproduccion"
)
archivos = list.files("files", pattern = ".csv", full.names = T)

produccion = lapply(archivos, 
                    function(x) {read.csv(x, col.names = nombres, fileEncoding = "Latin1") })
produccion = do.call(rbind, produccion)

# tenemos 2018 con 312 cultivos como el máximo año en número de cultivos reportados;
# 2019 tiene 293 cultivos reportados, mientras que 2020 y 2021 por la pandemia 
# apenas llegaron a 80
# tomaremos 2019 como el año para comparar con otros estados
names(produccion) |> dput()
produccion |> group_by(anio) |> count(nomcultivo) |> count(name = "num_cultivos_reportados")


produccion |> filter(anio == 2019) |>
  group_by(nomestado, nomcultivo) |> 
  summarise(total = sum(volumenproduccion)) |>
  ungroup() |>
  group_by(nomcultivo) |> filter(total == max(total)) |> filter(nomestado == "Veracruz")


# primeros lugares de producción
produccion |> filter(anio == 2019) |>
    group_by(nomestado, nomcultivo) |> 
    summarise(total = sum(volumenproduccion)) |>
    ungroup() |>
    group_by(nomcultivo) |> slice_max(total, n = 3) |> mutate(ranking = row_number()) 
  
  
produccion %>% filter(nomestado == "Veracruz") %>%
  group_by(anio,  nomcultivo, nomddr) %>% 
  summarise(total_toneladas = sum(volumenproduccion, na.rm = T)) |> 
  ungroup()
