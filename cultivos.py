import plotly.express as px
import pandas as pd

q = pd.read_csv("~/oh_my_gits/cultivos_veracruz/cultivoswide.csv")
q = q.sort_values(by = ["anio", "nomddr"])

fig = px.line(q, x = "anio", y = "avena_forrajera_en_verde", color = "nomddr", title = "Cultivos por DDR")

ea = []
for i in q.keys()[range(4,40)]:
    ea.append(dict(
         args=[{"y" :[q[i]]}],
         label=i,
         method="restyle"
    ))



fig.update_layout(
     updatemenus=[
         dict(
             buttons= ea,
             direction="down",
             pad={"r": 10, "t": 10},
             showactive=True,
             x=0.1,
             xanchor="left",
             y=1.1,
             yanchor="top"
         ),
     ]
)

fig.show()
#fig.write_html("cultispy.html")

