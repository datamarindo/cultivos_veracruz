library(plotly)
library(rjson)
library(tidyr)
library(dplyr)

# Para descarga del SIAP
# for i in $(seq 2003 2021); \ 
# do wget \ 
# $(printf http://infosiap.siap.gob.mx/gobmx/datosAbiertos/ProduccionAgricola/Cierre_agricola_mun_%s.csv $i); done

nombres = c("anio", "idestado", "nomestado", "idddr", "nomddr", "idcader", 
            "nomcader", "idmunicipio", "nommunicipio", "idciclo", "nomcicloproductivo", 
            "idmodalidad", "nommodalidad", "idunidadmedida", "nomunidad", 
            "idcultivo", "nomcultivo", "sembrada", "cosechada", "siniestrada", 
            "volumenproduccion", "rendimiento", "precio", "valorproduccion"
)
archivos = list.files("oh_my_gits/cultivos_veracruz/files", pattern = ".csv", full.names = T)

produccion = lapply(archivos, 
                    function(x) {read.csv(x, col.names = nombres, fileEncoding = "Latin1") })
produccion = do.call(rbind, produccion)


q = produccion %>% filter(nomestado == "Veracruz" & anio == 2019) %>%
  group_by(anio,  nomcultivo, nomddr) %>% 
  summarise(total_toneladas = sum(volumenproduccion, na.rm = T))
q = q %>% pivot_wider(names_from = nomddr, values_from = total_toneladas, values_fill = 0) |> 
  pivot_longer(cols = 3:14,  values_to = "total_toneladas", names_to = "nomddr") |> arrange(nomddr)

orden = c("Pánuco", "Coatepec", "Jáltipan", "La Antigua", "Ciudad Alemán", 
          "Fortín", "Veracruz", "San Andrés Tuxtla", "Martínez de la Torre", 
          "Choapas", "Huayacocotla", "Tuxpan")

# EL GEOJSON TIENE QUE SEGUIR LA RIGHT HAND RULE !!!
counties <- rjson::fromJSON(file="ddr_right_hand.geojson")
q$nomddr = factor(q$nomddr, levels = orden)

# SI SON MÁS DE 50 EL DROPDOWN DEJA DE FUNCIONAR en móvil
lostop = q |> group_by(nomcultivo) |> summarise(total = sum(total_toneladas)) |> ungroup() |>
  slice_max(order_by = total, n = 30) |> magrittr::use_series(nomcultivo)

q = q |> filter(nomcultivo %in% lostop)
g <- list(
  fitbounds = "locations" ,
  visible = TRUE,
  showland = TRUE,
  landcolor = toRGB("gray95")
)

fig <- plot_ly(transforms = list(list(type = "filter", 
                       target = ~nomcultivo,
                       operation = "=",
                       value = "Café cereza")))

fig <- fig %>% add_trace(
  type = "choropleth",
  geojson = counties,
  locations = q$nomddr,
  data = q,
  z = q$total_toneladas,
  colorscale="Viridis",
  featureidkey = "properties.Name"
)

fig <- fig %>% colorbar(title = "Producción total, toneladas", orientation = "h")

fig <- fig %>% layout(
  title = "Producción de cultivos, año 2020"
)

fig <- fig %>% layout(
  geo = g,
  updatemenus = list(
    list(type = "dropdown",
         active = 0,
         buttons = apply(as.data.frame(sort(unique(q$nomcultivo))), 1,
                         function(x) list(method = "restyle", 
                                          args = list("transforms[0].value", x), label = x))))
)

fig = config(fig, locale = "es")

htmlwidgets::saveWidget(partial_bundle(fig, minified = TRUE), 
                        "cultivos_veracruz_mapa_30.html", selfcontained = TRUE)
