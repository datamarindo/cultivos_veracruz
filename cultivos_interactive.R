library(dplyr)
library(ggplot2)
library(plotly)
library(tidyr)
library(htmlwidgets)
# Para descarga del SIAP
# for i in $(seq 2003 2021); \ 
# do wget $(printf \ 
# http://infosiap.siap.gob.mx/gobmx/datosAbiertos/ProduccionAgricola/Cierre_agricola_mun_%s.csv $i); done

prona = png::readPNG("oh_my_gits/cultivos_veracruz/files/INECOL.png")
nombres = c("anio", "idestado", "nomestado", "idddr", "nomddr", "idcader", 
            "nomcader", "idmunicipio", "nommunicipio", "idciclo", "nomcicloproductivo", 
            "idmodalidad", "nommodalidad", "idunidadmedida", "nomunidad", 
            "idcultivo", "nomcultivo", "sembrada", "cosechada", "siniestrada", 
            "volumenproduccion", "rendimiento", "precio", "valorproduccion"
)

archivos = list.files("oh_my_gits/cultivos_veracruz/files", pattern = ".csv", full.names = T)

produccion = lapply(archivos, 
                    function(x) {read.csv(x, col.names = nombres, fileEncoding = "Latin1") })
produccion = do.call(rbind, produccion)


q = produccion %>% filter(nomestado == "Veracruz") %>%
  group_by(anio,  nomcultivo, nomddr) %>% 
  summarise(total_toneladas = sum(volumenproduccion, na.rm = T)) |> 
  ungroup()



# data frame tiene que estar llena de 0,0,0,0,0,0,0,0: no puede haber vacíos, si no se desordena
q = q %>% pivot_wider(names_from = nomddr, values_from = total_toneladas, values_fill = 0) |> 
  pivot_longer(cols = 3:14,  values_to = "total_toneladas", names_to = "nomddr") |> arrange(nomddr)
lostop = q |> group_by(nomcultivo) |> summarise(total = sum(total_toneladas)) |> ungroup() |>
  slice_max(order_by = total, n = 30) |> magrittr::use_series(nomcultivo)


q %>% pivot_wider(names_from = nomcultivo, values_from = total_toneladas, values_fill = 0) |>
  janitor::clean_names() 

q = q |> filter(nomcultivo %in% lostop & anio > 2010)

q$nomcultivo = gsub(" de azúcar|africana o | \\(.*| y praderas", "", q$nomcultivo) 


q %>% pivot_wider(names_from = nomcultivo, values_from = total_toneladas, values_fill = 0) |> 
  janitor::clean_names() |>
  write.csv("cultivoswide.csv")
pq = q |>  plot_ly(x = ~anio, 
                   y = ~total_toneladas, 
                   color = ~nomddr, #height = 550,
                   transforms = list(list(type = "filter", 
                                          target = ~nomcultivo,
                                          operation = "=",
                                          value = "Café cereza"))) %>% 
  layout(title = "Cultivos por Distrito de Desarrollo Rural", 
         margin = list(l = 150),
         images = list(source = raster2uri(as.raster(prona)),
                       x = 0, y = 1, 
                       sizex = .2, sizey = .2,
                       xref = "paper", yref = "paper",
                       opacity = 0.4),
         xaxis = list(title = ""), yaxis = list(title = "Toneladas"),
         annotations = 
           list(x = 1, y = 0, 
                text = "Fuente: Sistema de Información Agroalimentaria y Pesquera", 
                showarrow = F, xref='paper', yref='paper', 
                xanchor='right', yanchor='auto', xshift=0, yshift=0,
                font=list(size=15)),
         updatemenus = list(
           list(type = "dropdown", #pad = list(t = -20), x = -0.19,
                #font = list(size = 11 ),
                active = 0, bgcolor = "gray80", direction = "down",
                buttons = apply(as.data.frame(sort(unique(q$nomcultivo))), 1,
                                function(x) list(method = "restyle", 
                                                 args = list("transforms[0].value", x),
                                                        label = x)))
           ))

pq = add_paths(pq)

pq = config(pq, locale = "es")


htmlwidgets::saveWidget(partial_bundle(pq, minified = FALSE), 
                        "cultivos_veracruz_30.html", selfcontained = TRUE)


